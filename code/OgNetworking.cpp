//COPYRIGHT 2020/2021- OCACHO GAMES STUDIO - DAVID MARTINEZ GARCIA

#include "OgNetworking.h"
#include <iostream>
#include <stdio.h>
#include <functional>
#include <string.h>

//-----------------------------------------------------------------------------
//-------------------------------------

OgNetworking::OgNetworking()
	:mb_isHosting(false)
	,mb_isConnected(false)
{
	m_usersMap = {};
	m_eventList.num_messages = 0;

  	m_hashLambdasEvents = 
	{
    	{OG_NETWORK_DATA_IDENTIFY,[=](int id,char username[]){ updateReceiveIdentifyEvent(id,username);} },
   		{OG_NETWORK_DATA_DISCONNECT,[=](int id,char username[]){ updateReceiveDisconnectEvent(id,username);} },
    	{OG_NETWORK_DATA_USER,[=](int id,char username[]){ updateReceiveUserEvent(id,username);} }
	};	
}

//-----------------------------------------------------------------------------
//-------------------------------------

int OgNetworking::init()
{
	if(enet_initialize() !=0)
	{
		fprintf (stderr, "An error occurred while initializing ENet.\n");
		return EXIT_FAILURE;
	}
	atexit(enet_deinitialize);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::update()
{
	if(!mb_isConnected)
		return;

	while(enet_host_service(peer,&event,0) > 0)
	{
		switch (event.type)
		{
			case ENET_EVENT_TYPE_CONNECT:
			{
				updateConnectEvent();
				break;
			}
			case ENET_EVENT_TYPE_RECEIVE:
			{
				updateReceiveEvent();
				break;
			}

			case ENET_EVENT_TYPE_DISCONNECT:
			{
				updateDisconnectEvent();
				break;
			}
		}
	}

}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateConnectEvent()
{
	puts("A new client connected");

	m_idCounter++;

	//Keeping the track of the clients peer as server
	m_serverPeersMap.insert(std::make_pair(event.peer,m_idCounter));

	//Allocate memory for data
	event.peer->data = (char*) malloc (255 * sizeof(char));

	//Send the identification info to the new client to set his new id
	sendIdentificationPacketToClient();
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateUsernameEvent(const char* p_data)
{
	if(mb_isHosting)
	{
		int dataType;
		char aux[80] = {'\0'};
		char* clientUsername;

		sscanf(p_data,"%i|%s", &dataType,&aux);

		if(dataType == OG_NETWORK_DATA_USERNAME)
		{
			printf("Setting new client username... \n");
			
			//Allocating memory
			clientUsername = (char*) malloc (strlen(aux) * sizeof(char));

			//Filling the clientUsername
			for(unsigned int i=0; i < strlen(aux)+1; i++)
			{
				clientUsername[i] = aux[i];
			}

			m_usersMap.insert(std::make_pair(m_idCounter,clientUsername));

			printf("Client username: %s \n", clientUsername);

			//Updating m_usersMap for all the users
			sendUpdateUsersMap();
		}
	}
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateReceiveEvent()
{
	m_eventList.messages[m_eventList.num_messages].sender.enet_peer = event.peer;
	m_eventList.messages[m_eventList.num_messages].size = event.packet->dataLength;
	m_eventList.messages[m_eventList.num_messages].data = (char*) malloc (event.packet->dataLength * sizeof(char));

	//Once the memory is allocated, fill the buffer
	for(unsigned int i=0; i< event.packet->dataLength; i++)
	{
		m_eventList.messages[m_eventList.num_messages].data[i] = event.packet->data[i];
	}

	//In case we are hosting, the received packet needs to be sent to all the clients
	if(mb_isHosting)
	{
		broadcastPacket((char*)event.packet->data,strlen((char*)event.packet->data),event.peer);

		//Also set the new username connected
		updateUsernameEvent((char*)event.packet->data);
	}

	int dataType;
	int id;
	char username[80] = {};

	sscanf(m_eventList.messages[m_eventList.num_messages].data, "%i|%i|%s",&dataType,&id,&username);
	
	if(m_hashLambdasEvents.count(dataType))
	{
		std::__invoke(m_hashLambdasEvents[dataType],id,username);
	}

	m_eventList.num_messages++;

	//Destroy the packet once the process is done to a fast communication
	enet_packet_destroy(event.packet);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateReceiveIdentifyEvent(int id, char username[])
{
	m_id = id;
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateReceiveDisconnectEvent(int id, char username[])
{
	if(m_usersMap.count(id))
	{
		m_usersMap.erase(id);
	}
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateReceiveUserEvent(int id, char username[])
{
	if(!m_usersMap.count(id) && id != m_id)
	{
		char* usernameReference;

		//Allocating memory
		usernameReference = (char*) malloc (strlen(username) * sizeof(char));

		//Filling the clientUsername
		for(unsigned int i=0; i < strlen(username)+1; i++)
		{
			usernameReference[i] = username[i];
		}

		m_usersMap.insert(std::make_pair(id,usernameReference));
	}
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::updateDisconnectEvent()
{
	//This code is only executed by the server
	printf("A client has disconnected. \n");

	if(m_usersMap.count(m_serverPeersMap[event.peer]))
	{
		m_usersMap.erase(m_serverPeersMap[event.peer]);

		sendDisconnectionPacketToClients();
	}
}

//-----------------------------------------------------------------------------
//-------------------------------------

int OgNetworking::createHost(unsigned int p_port, unsigned int p_maxConnections, const char* p_username)
{
	ENetAddress address;

	address.host = ENET_HOST_ANY;
	address.port = p_port;

	peer = enet_host_create(&address,p_maxConnections,1,0,0);

	if(peer == NULL)
	{
		fprintf (stderr, "An error occurred while creating ENet host.\n");
		return EXIT_FAILURE;
	}

	mb_isHosting = true;
	mb_isConnected = true;
	m_id = 1;
	m_username = p_username;
	m_idCounter = 1;

	return EXIT_SUCCESS;
}

//-----------------------------------------------------------------------------
//-------------------------------------

int OgNetworking::connect(const char* p_addressToConnect, unsigned int port, const char* p_username)
{
	ENetAddress address;
	ENetEvent clientEvent;

	enet_address_set_host(&address,p_addressToConnect);
	address.port = port;

	//Address is null, we don't want to host any game
	peer = enet_host_create(NULL, 1 ,1 , 0 ,0);

	if(peer == NULL)
	{
		fprintf (stderr, "An error occurred while creating ENet client.\n");
		return EXIT_FAILURE;
	}

	//Set the server we are connecting to
	server = enet_host_connect(peer,&address,1,0);

	if(server == NULL)
	{
		fprintf (stderr, "No avaliable server for initializing ENet connection!.\n");
		return EXIT_FAILURE;
	}

	if(enet_host_service(peer,&clientEvent,5000) > 0 &&
		clientEvent.type == ENET_EVENT_TYPE_CONNECT)
	{
		puts("Connection succeded");
		mb_isConnected = true;
		m_username = p_username;

		//Send username packet
		char buffer [80] = {'\0'};
		snprintf(buffer,sizeof(buffer), "%i|%s",OG_NETWORK_DATA_USERNAME,p_username);

		sendPacketToServer(buffer,strlen(buffer) + 1);
	}else
	{
		enet_peer_reset(server);
		fprintf (stderr, "Connection failed!.\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::sendPacket(const char* p_data)
{		
	if(mb_isHosting)
		broadcastPacket(p_data,strlen(p_data) + 1,NULL);
	else
		sendPacketToServer(p_data,strlen(p_data) + 1);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::broadcastPacket(const char* p_data, size_t p_size, ENetPeer* p_clientToAvoid)
{
	ENetPacket* packet = enet_packet_create(p_data,p_size, ENET_PACKET_FLAG_RELIABLE);

	if(p_clientToAvoid == NULL)
	{
		enet_host_broadcast(peer,0,packet);
	}
	else
	{
		for(int i=0; i < peer->peerCount; i++)
		{
			if(peer && &peer->peers[i] &&  &peer->peers[i] != p_clientToAvoid)
			{
				enet_peer_send(&peer->peers[i],0,packet);
			}
		}
	}
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::sendPacketToServer(const char* p_data, size_t p_size)
{
	ENetPacket* packet = enet_packet_create(p_data,p_size, ENET_PACKET_FLAG_RELIABLE);

	enet_peer_send(server,0,packet);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::sendPacketTo(ENetPeer* p_peer, const char* p_data, size_t p_size)
{
	ENetPacket* packet = enet_packet_create(p_data,p_size, ENET_PACKET_FLAG_RELIABLE);

	enet_peer_send(p_peer,0,packet);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::sendIdentificationPacketToClient()
{	
	//Sending the client his id
	char buffer[5] = {'\0'};
	snprintf(buffer,sizeof(buffer),"%i|%i",OG_NETWORK_DATA_IDENTIFY,m_idCounter);

	sendPacketTo(event.peer,buffer,strlen(buffer) + 1);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::sendDisconnectionPacketToClients()
{
	char buffer[5] = {'\0'};
	snprintf(buffer,sizeof(buffer),"%i|%i",OG_NETWORK_DATA_DISCONNECT,m_serverPeersMap[event.peer]);

	ENetPacket* packet = enet_packet_create(buffer,strlen(buffer) + 1, ENET_PACKET_FLAG_RELIABLE);

	sendPacket(buffer);
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::sendUpdateUsersMap()
{
	//Server
	char buffer[80] = {'\0'};
	snprintf(buffer,sizeof(buffer),"%i|%i|%s",OG_NETWORK_DATA_USER,1,m_username);

	ENetPacket* packet = enet_packet_create(buffer,strlen(buffer) + 1, ENET_PACKET_FLAG_RELIABLE);

	sendPacketTo(event.peer, buffer ,strlen(buffer)+1);

	//Clients
	if(!m_usersMap.empty())
	{
		for(auto const& user : m_usersMap)
		{
			char buffer[80] = {'\0'};
			snprintf(buffer,sizeof(buffer),"%i|%i|%s",OG_NETWORK_DATA_USER,user.first,user.second);

			ENetPacket* packet = enet_packet_create(buffer,strlen(buffer) + 1, ENET_PACKET_FLAG_RELIABLE);

			broadcastPacket(buffer ,strlen(buffer)+1);
			
		}
	}
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::disconnect()
{
	if(!mb_isConnected || mb_isHosting )
		return;

	enet_peer_disconnect(server,0);

	while(enet_host_service(peer,&event,1000) > 0)
	{
		switch (event.type)
		{
			case ENET_EVENT_TYPE_RECEIVE:
				enet_packet_destroy(event.packet);
				break;
			case ENET_EVENT_TYPE_DISCONNECT:
			{
				puts("Disconnection succeded.");
				mb_isConnected = false;
				break;
			}
		}
	}

	mb_isConnected = false;
}

//-----------------------------------------------------------------------------
//-------------------------------------

void OgNetworking::cleanUp()
{
	disconnect();
	
	if(peer != nullptr)
		enet_host_destroy(peer);

	m_eventList.num_messages = 0;

	mb_isHosting = false;
	mb_isConnected = false;
	m_usersMap.clear();
	m_usersMap = {};
}