# OCACHO NETWORKING ENGINE

### Ocacho Networking engine ![Version](https://img.shields.io/badge/version-v1.0-green)

<img src="./ocacho.png" width=50%>

# What is it?

Ocacho Networking engine is an UPD Server/Client structure Multithreading networking engine. Based on C++ &amp; ENet.

###### Features

- **Event handler**: The engine is based on events that peers can send each other. The main feature is the event list. With that list you will be able to loop through all your events that other peers have sent to you. Avoiding coupling.

- **Sending packets**: The engine is based on sending packets. You will be able to send a packet through the net using sendPacket().
You can check an example of how to use it in the main.cpp

- **Server/Client structure**: Create servers and joining them as clients. The server size is variable.

- **Map of users**: Each peer has its own map of users. This map give us the info of all the players connected to our session, as client or server you will be able to loop through the connected users. The map has [ID, username]

- **Total players**: You can check at any moment the total of players that are in your session

- **Disconnection**: The disconnection event is handle. The users map will erase the user disconnected.


###### Example

The engine has an example of a simple 2 players game -> main.cpp

This is just an example. The engine can hold more than 2 players.

<br>

## How to install and use Ocacho networking engine

Make sure you have ENet and haguichi/hamachi installed on your computer.

In case you want to test the example you will need SFML too. Instructions below!

**Debian**

```bash
    -Enet- 
    sudo apt install libenet-dev

    -SFML-
    sudo apt-get install libsfml-dev
    
    -Hamachi- 
    sudo add-apt-repository -y ppa:webupd8team/haguichi
    sudo apt update
    sudo apt install -y haguichi
    sudo systemctl start logmein-hamachi
```

**Arch**

```bash
    -Enet- 
    sudo pacman -S enet

    -SFML-
    sudo pacman -S sfml
    
    -Hamachi- 
    sudo pacman -S haguichi
    sudo systemctl start logmein-hamachi 
```
Now, run ./build and that's all!

**Important**

In case you wanna use this engine on your projects, make sure you have the same config as this project. Include the ./cmake_modules/FindENet.cmake and the CMakeLists from ./ and ./code

<br>

## Social medias

**Portfolio:** http://davidmartinez.125mb.com/ <br>
**Twitter:** https://twitter.com/DavidMart_6 <br>
**LinkedIn:** https://www.linkedin.com/in/david-mart%C3%ADnez-garc%C3%ADa-62b401188/
