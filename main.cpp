#include <iostream>
#include <stdio.h>
#include <string.h>
#include "SFML/Graphics.hpp"
#include "code/OgNetworking.h"

#define kVel 5

//Circles
sf::CircleShape* circle1 = nullptr;
sf::CircleShape* circle2 = nullptr;

//Our instance circle that we will move
sf::CircleShape* playerCircle = nullptr;
//The circle of our friend connected
sf::CircleShape* friendCircle = nullptr;

sf::RenderWindow window;

//Vector to check our previos position for only send a new position in case we are moving, for optimization
sf::Vector2f myPreviousPosition;

//Option chosen, server o client
char option;

//Reference to our networking engine
OgNetworking* network;

//Initialize and setup SFML stuff
void setupSFML();
//Update sfml events, key inputs
void updateSFML();

//Initialize the network as server or client and choosing our player circle 
void setupOgNetwork();
//Update the receiving networking events
void updateOgNetwork();

//This is just for debug, this shows some of the funcionalities of the engine
void debugNetworkFunctions();

//----------------------------------------------------------------------------------------------------------
//-------------------------------------

int main() {

	option = {};
	do
	{
		puts("Server(s) or Client(c): ");
		std::cin >> option;

	}while (option != 's' && option != 'c');

	network = new OgNetworking();
	network->init();

	setupSFML();
	setupOgNetwork();

  	//Game loop
  	while (window.isOpen()) {
		
		network->update();

		myPreviousPosition = playerCircle->getPosition();
		
		updateSFML();
		updateOgNetwork();

		window.clear();
		window.draw(*circle1);
		window.draw(*circle2);
		window.display();
	}

  return 0;
}

//----------------------------------------------------------------------------------------------------------
//-------------------------------------

void setupSFML()
{
	window.create(sf::VideoMode(1280, 720), "Ocacho netowrking engine. Example");

	circle1 = new sf::CircleShape(50); 
	circle1->setScale(1, 1);
	circle1->setFillColor(sf::Color::Red);

	circle2 = new sf::CircleShape(50); 
	circle2->setScale(1, 1);
	circle2->setFillColor(sf::Color::Blue);
}

//----------------------------------------------------------------------------------------------------------
//-------------------------------------

void updateSFML()
{
	sf::Event event;
	while (window.pollEvent(event)) {

		switch (event.type) {

			//Si se recibe el evento de cerrar la ventana la cierro
			case sf::Event::Closed:
				network->cleanUp();
				window.close();
				break;

			//Se pulsó una tecla, imprimo su codigo
			case sf::Event::KeyPressed:

				//Verifico si se pulsa alguna tecla de movimiento
				switch (event.key.code) {

					case sf::Keyboard::D:
					{
						playerCircle->move(kVel, 0);
						break;
					}

					case sf::Keyboard::A:
					{
						playerCircle->move(-kVel, 0);
						break;
					}

					case sf::Keyboard::W:
					{
						playerCircle->move(0, -kVel);
						break;
					}

					case sf::Keyboard::S:
					{
						playerCircle->move(0, kVel);
						break;
					}

					case sf::Keyboard::Escape:
					{
						window.close();
						break;
					}
				}
		}
	}
}

//----------------------------------------------------------------------------------------------------------
//-------------------------------------

void setupOgNetwork()
{
	if(option == 's')
	{
		//Server connection 

		//Create the server, as host, ip set automatically
		if(network->createHost(8080,6,"Server") == EXIT_SUCCESS)
		{
			playerCircle = circle1;
			friendCircle = circle2;

			puts("Server created successfully");

		}
		else
		{
			puts("Error creating the server");
		}
	}
	else
	{
		//Client connection

		//Joining a host at one address and port
		//Use here your haguichi/hamachi ip like 25.12.......
		if(network->connect("XX.XX.XXX.XXX",8080,"Cliente") == EXIT_SUCCESS)
		{
			playerCircle = circle2;
			friendCircle = circle1;
		}
	}
}

//----------------------------------------------------------------------------------------------------------
//-------------------------------------

void updateOgNetwork()
{
	//Taking the receiving events list 
	og_network_events* eventList = network->getEventList();

	for(int i=0; i< eventList->num_messages; i++)
	{
		int dataType;
		float x,y;
		sscanf(eventList->messages[i].data, "%i|%f|%f",&dataType,&x,&y);

		//Ensure we receive the event we want
		if(dataType == OG_NETWORK_DATA_POSITION_XY)
		{
			//Set the other player circle position
			friendCircle->setPosition(x,y);
		}
	}

	//Empty the messages after loop through them
	eventList->num_messages = 0;


	//Optimization: we don't want to send all the packets only if our position has changed
	if(myPreviousPosition != playerCircle->getPosition())
	{
		//Buffer to stock our info
		char buffer[100] = {'\0'};
		snprintf(buffer,sizeof(buffer), "%i|%f|%f",OG_NETWORK_DATA_POSITION_XY,playerCircle->getPosition().x, playerCircle->getPosition().y);

		network->sendPacket(buffer);
	}
}

//----------------------------------------------------------------------------------------------------------
//-------------------------------------

void debugNetworkFunctions()
{
	network->getUsersMap(); //Return the users of the current session [ID, username]
	network->getUsername(); //Return my username
	network->getId(); //Return my id
	network->getCurrentUsersNumber(); //Numbers of the userst of the current session

	//You can check others functions checking the OgNetworkin.h
}